# ODM

### Installation
* Clone repository to your machine:
```bash
    git clone 
```

* Install dependencies:
```bash
    composer install
```

* Configure Environment:
```bash
    cp .env.example .env
    php artisan key:generate
```

* Migrate Database:
```bash
    php artisan migrate:fresh --path=database/migrations/system    
```

* Install Passport OAuth clients & keys:
```bash
    php artisan passport:install
```

* Set GIT hooks directory path:
```bash
    git config core.hooksPath .githooks
```
