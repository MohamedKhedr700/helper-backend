<?php

return [
    'category'     => 'category',
    'name_ar'      => 'name ar',
    'name_en'      => 'name en',
    'industry_id'  => 'industry id',
    'description'  => 'description',
    'status'       => 'status',
    'photo'        => 'photo',
    'parent_id'    => 'category id',
];
