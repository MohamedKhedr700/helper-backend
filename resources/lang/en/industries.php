<?php

return[
    'industries'                      => 'industries',
    'industry'                        => 'industry',
    'change-status-industry-success'  => 'industry status changed successfully',
    'change-status-industry-fail'     => 'failed to change industry',
    'nameEn'                          => 'English Name',
    'nameAr'                          => 'Arabic Name',
];
