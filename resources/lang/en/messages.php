<?php

return [
    'create'            => [
        'success' => 'The :module created successfully.',
        'fail'    => 'Something went wrong when create the :module.',
    ],
    'update'            => [
        'success' => 'The :module updated successfully.',
        'fail'    => 'Something went wrong when update the :module.',
    ],
    'delete'            => [
        'success' => 'The :module deleted successfully.',
        'fail'    => 'Something went wrong when delete the :module.',
    ],
    'archive'           => [
        'success' => 'The :module archived successfully.',
        'fail'    => 'Something went wrong when archive the :module.',
    ],
    'restore'           => [
        'success' => 'The :module restored successfully.',
        'fail'    => 'Something went wrong when restore the :module.',
    ],
    'exception'         => [
        '404' => 'Document or file requested by the client was not found.',
        '403' => 'Access is denied.',
        '413' => 'Payload is too large.',
        '500' => 'Internal Server Error.',
    ],

    'cancel'            => [
        'success' => 'The pairing request has been canceled.',
        'fail'    => 'Something went wrong when canceling your request',
    ],
];
