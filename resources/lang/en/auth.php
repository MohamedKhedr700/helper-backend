<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'             => 'Username or Password is incorrect.',
    'throttle'           => 'Too many login attempts. Please try again in :seconds seconds.',
    'success'            => 'Operation successfully.',
    'deactivated'        => 'Your Account is deactivated for more information contact with the administration',
    'deactivated_client' => 'Deactivated ,Please contact your administrator',
    'trial_expired'      => 'Expired , Please contact Giraffe support',

];
