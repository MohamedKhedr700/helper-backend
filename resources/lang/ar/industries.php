<?php

return[
    'industries'                       => 'صناعات',
    'industry'                         => 'صناعة',
    'change-status-industry-success'   => 'تم تغير حالة الصناعة بنجاح',
    'change-status-industry-fail'      => 'لم يتم تغير حالة الصناعة',
    'nameEn'                           => 'الاسم باللغة لانجلزية',
    'nameAr'                           => 'الاسم باللغة بالعربية',
];
