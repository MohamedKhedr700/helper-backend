<?php
return [
    'category'     => 'الفئه',
    'name_ar'      => 'الاسم العربي',
    'name_en'      => 'الإسم الإنجليزي',
    'industry_id'  => 'الصناعه',
    'description'  => 'الوصف',
    'status'       => 'الحاله',
    'photo'        => 'الصوره',
    'parent_id'    => 'الفئه',
];
