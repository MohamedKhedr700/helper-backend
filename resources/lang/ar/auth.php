<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'             => 'بيانات الاعتماد هذه غير متطابقة مع البيانات المسجلة لدينا.',
    'throttle'           => 'عدد كبير جدا من محاولات الدخول. يرجى المحاولة مرة أخرى بعد :seconds ثانية.',
    'success'            => 'العملية تمت بنجاح.',
    'deactivated'        => 'تم إيقاف حسابك الشخصي. لمزيد من المعلومات برجاء التواصل مع مدير النظام.',
    'deactivated_client' => 'حساب معطل, منفضلك تواصل مع المسئول عن حسابك',
    'trial_expired'      => 'انتهت الصلاحيه, تواصل مع جيراف كود',

];
