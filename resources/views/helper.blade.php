<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>
<h2>Marota Register Form</h2>
<p> Filled By </p>
<table>
    <tr>
        <td>First Name</td>
        <th>{{$firstName}}</th>
    </tr>
    <tr>
        <td>Last Name</td>
        <th>{{$lastName}}</th>
    </tr>
    @if ($jobTitle !== null)
    <tr>
        <td>Job Title</td>
        <th>{{$jobTitle}}</th>
    </tr>
    @endif
    <tr>
        <td>Mobile</td>
        <th>{{$mobile}}</th>
    </tr>
    <tr>
        <td>Email</td>
        <th>{{$email}}</th>
    </tr>
    @if($notes !== null)
    <tr>
        <td>Registration Notes</td>
        <th>{{$notes}}</th>
    </tr>
    @endif
</table>

</body>
</html>
