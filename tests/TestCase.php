<?php

namespace Tests;

use Giraffe\Pos\Models\Pos;
use Giraffe\Role\Models\Role;
use Giraffe\User\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Giraffe\Provider\Models\Provider;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $users;

    public function setUp(): void
    {
        parent::setUp();

        $this->withoutEvents();
        $this->withoutJobs();
    }

    public function createRowUsers($count = 2, $data = [])
    {
        $this->users = User::factory()->count($count)->create($data);

        return $this;
    }

    public function createRowUser($data = [])
    {
        $this->users = $this->createRowUsers(1, $data)->apply()->first();

        return $this;
    }

    public function createUsers($count = 2, $role = 'administrator', $data = [])
    {
        $users = $this->createRowUsers($count, $data)->apply();

        $this->storeRole($role, $users->first()->provider_id);

        $users->each(function ($user) use ($role) {
            $user->attachRole($role);
        });

        $this->users = ($count == 1) ? $users->first() : $users;

        return $this;
    }

    public function createUser($role = 'administrator', $data = [])
    {
        return $this->createUsers(1, $role, $data);
    }

    public function createOwner($data = [])
    {
        return $this->createUser('owner', $data);
    }

    public function createAdmin($data = [])
    {
        return $this->createUser('administrator', $data);
    }

    public function storeRole($role, $parentId, $forProvider = true)
    {
        if (in_array($role, ['owner', 'administrator'])) {
            return $this->storeSystemRole($role);
        }

        return $this->storeUserRole($role);
    }

    public function storeSystemRole($role)
    {
        return Role::factory()->create([
            'name'           => Str::slug($role, '-'),
            'display_name'   => $role,
            'description'    => 'The site ' . $role,
            'deletable'      => 0,
        ]);
    }

    public function storeUserRole($role)
    {
        return Role::factory()->create([
            'name'           => Str::slug($role, '-'),
            'display_name'   => $role,
            'description'    => 'The site ' . $role,
        ]);
    }

    public function with($something)
    {
        $this->{$something}();

        return $this;
    }

    public function apply()
    {
        return $this->users;
    }

    protected function tearDown(): void
    {
        $this->beforeApplicationDestroyed(function () {
            DB::disconnect();
        });

        parent::tearDown();
    }
}
