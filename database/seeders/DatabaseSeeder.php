<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Nwidart\Modules\Facades\Module;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTableSeeder::class);
        $this->call(LaratrustSeeder::class);

        $modulesSeedes = collect(Module::all())->map(function ($module) {
            return config('modules.namespace') . '\\' . $module->getName() . '\\Database\Seeders\\' . $module->getName() . 'DatabaseSeeder';
        })->values()->toArray();
        $this->call($modulesSeedes);
    }
}
