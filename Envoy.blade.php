@if ($branch != 'master')
    @servers(['development' => 'deployer@212.70.49.119 -p 12012'])
@endif

@story('deploy')
    @if ($branch == 'develop')
        deploy_develop
    @endif
    @if ($branch == 'test')
        deploy_test
    @endif
    @if ($branch == 'stage')
        deploy_stage
    @endif
    @if ($branch == 'uat')
        deploy_uat
    @endif
    installation
@endstory


@task('deploy_develop', ['on' => 'development'])
    cd /var/www/odm/dev/api
    git reset --hard origin/develop
    git pull origin develop
@endtask

@task('deploy_test', ['on' => 'development'])
    cd /var/www/odm/test/api
    git reset --hard origin/test
    git pull origin test
@endtask

@task('deploy_stage', ['on' => 'development'])
    cd /var/www/odm/stage/api
    git reset --hard origin/stage
    git pull origin stage
@endtask

@task('deploy_uat', ['on' => 'development'])
    cd /var/www/odm/uat/api
    git reset --hard origin/uat
    git pull origin uat
@endtask


@task('installation')
    @if ($branch == 'develop')
        cd /var/www/odm/dev/api
    @endif
    @if ($branch == 'test')
        cd /var/www/odm/test/api
    @endif
    @if ($branch == 'stage')
        cd /var/www/odm/stage/api
    @endif
    @if ($branch == 'uat')
        cd /var/www/odm/uat/api
    @endif
    composer install --no-interaction
    php artisan migrate --seed
    php artisan cache:clear
    php artisan config:clear
    php artisan view:clear
    php artisan l5-swagger:generate
@endtask
