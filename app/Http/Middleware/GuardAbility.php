<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Laratrust\Middleware\LaratrustAbility;

class GuardAbility
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param $abilities
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$abilities)
    {
        $roles       = [];
        $permissions = [];

        for ($i=0; $i < count($abilities); $i += 3) {
            if (in_array($abilities[$i], array_keys(config('auth.guards')))) {
                if (auth($abilities[$i])->check()) {
                    $roles       = $abilities[$i + 1] ?? [];
                    $permissions = $abilities[$i + 2] ?? [];

                    break;
                }
            }
        }

        return app(LaratrustAbility::class)->handle($request, function ($request) use ($next) {
            return $next($request);
        }, $roles, $permissions);
    }
}
