<?php

namespace App\Http\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ClientConfigTransformer extends TransformerAbstract
{
    public function transform($config)
    {
        return [
            'name'           => tenant()->name,
            'logo'           => ($config && $config->logo) ? route('image.show', ['settings', $config->logo]) : null,
            'primaryColor'   => $config->primary_color ?? null,
            'secondaryColor' => $config->secondary_color ?? null,
            'status'         => (bool) tenant()->status,
            'expired'        => (bool) (tenant()->contract_type != 'lifetime' && Carbon::parse(tenant()->contract_expiration) > now()) ? true : false,
        ];
    }
}
