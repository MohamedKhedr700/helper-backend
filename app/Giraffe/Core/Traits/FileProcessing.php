<?php

namespace Giraffe\Core\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

trait FileProcessing
{
    public static function uploadFile($dir, $file)
    {
        $path     = implode('/', ['media', $dir]);
        $fileName = str_replace($path.'/', '', $file->store($path));

        try {
            self::addThumbnail($path, $fileName, $file);
        } catch (\Exception $exception) {
            Log::debug($exception->getMessage());
        }

        return $fileName;
    }

    protected static function addThumbnail(string $path, string $fileName, UploadedFile $file)
    {
        if (!\File::exists($path . '-thumbnail-sm')) {
            mkdir($path . '-thumbnail-sm', 0775, true);
            mkdir($path . '-thumbnail-md', 0775, true);
            mkdir($path . '-thumbnail-lg', 0775, true);
        }
        Storage::put($path . '-thumbnail-sm/' . $fileName, Image::make($file->getRealPath())->resize(150, 93, function ($constraint) {
            $constraint->aspectRatio();
        })->response()->getContent());
        Storage::put($path . '-thumbnail-md/' . $fileName, Image::make($file->getRealPath())->resize(300, 185, function ($constraint) {
            $constraint->aspectRatio();
        })->response()->getContent());
        Storage::put($path . '-thumbnail-lg/' . $fileName, Image::make($file->getRealPath())->resize(550, 340, function ($constraint) {
            $constraint->aspectRatio();
        })->response()->getContent());
    }

    public static function generateImageURL($path)
    {
        $image    = [];
        $fullPath = storage_path('media/'.$path);
        if (\File::exists($fullPath)) {
            $image['file'] = \File::get($fullPath);
            $image['type'] = \File::mimeType($fullPath);

            return $image;
        } else {
            throw new FileNotFoundException();
        }
    }

    public static function unlinkURL($dir, $file)
    {
        $fullPath = storage_path("media/${dir}/${file}");
        if (\File::exists($fullPath)) {
            \File::delete($fullPath);
        }

        return null;
    }
}
