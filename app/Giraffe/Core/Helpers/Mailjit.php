<?php

namespace Giraffe\Core\Helpers;

use Mailjet\Client;
use Mailjet\Resources;

class Mailjit
{
    protected $mail;

    public function __construct()
    {
        $this->mail = new Client(getenv('MAILJET_APIKEY'), getenv('MAILJET_APISECRET'), true, ['version' => 'v3.1']);
    }

    public function send($to, $subject, $body)
    {
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => getenv('MAILJET_SENDER_EMAIL'),
                        'Name'  => getenv('MAILJET_SENDER_NAME'),
                    ],
                    'To' => [
                        [
                            'Email' => $to,
                            'Name'  => $to,
                        ],
                    ],
                    'Subject'  => $subject,
                    'HTMLPart' => $body,
                ],
            ],
        ];
        $this->mail->post(Resources::$Email, ['body' => $body]);
    }
}
