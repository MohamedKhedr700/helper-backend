<?php

namespace Giraffe\Core\Repositories;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Giraffe\Core\Repositories\Contracts\BaseRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

abstract class Repository implements BaseRepository
{
    /**
     * Repository model.
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * return query builder.
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function builder(): Builder
    {
        return $this->model->query();
    }

    /**
     * prefix the given column with model table name.
     * @param  string $column
     * @return string
     */
    protected function prefixTable(string $column): string
    {
        return (Str::contains($column, '.')) ? $column : $this->model->getTable() . '.' . $column;
    }

    /**
     * Set select columns.
     * @param  array  $columns
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function select(array $columns = ['*']): Builder
    {
        return $this->builder()->selectRaw(implode(',', $columns));
    }

    /**
     * Load translations data if exist.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function loadTranslations(Builder $query): Builder
    {
        if (method_exists($this->model, 'translations')) {
            $query->with('translations');
        }

        // load translations by joins if translations eloquent relationship not exist.
        if (method_exists($this->model, 'scopeTranslations') && !array_key_exists('translations', $query->getEagerLoads())) {
            $query->translations();
        }

        return $query;
    }

    /**
     * Build Retrieve query.
     * @param array $columns
     * @param array $options
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function retrieveQuery(array $columns = ['*'], array $options = []): Builder
    {
        $builder        = $this->select($columns);
        $sort           = $options['filters']['sort'] ?? config('app.order_by');
        $direction      = $options['filters']['direction'] ?? config('app.direction');
        $sortArray      = is_array($sort) ? $sort : [$sort];
        $directionArray = is_array($direction) ? $direction : [$direction];
        foreach ($sortArray as $key => $sort) {
            $builder->orderBy($sort, $directionArray[$key] ?? config('app.direction'));
        }
        $builder->limit($options['filters']['perPage'] ?? -1);

        if (key_exists('joins', $options) && is_array($options['joins']) && count($options['joins'])) {
            foreach ($options['joins'] as $join) {
                $builder->{$join}();
            }
        }

        if (in_array('distinct', $options) && $options['distinct']) {
            $builder->distinct();
        }

        $builder = $this->loadTranslations($builder);

        if (key_exists('filters', $options) && Arr::except($options['filters'], ['sort', 'direction'])) {
            $builder->filter($options['filters'] ?? []);
        }

        if (key_exists('groupBy', $options)) {
            $builder->groupBy($options['groupBy']);
        }

        return $builder;
    }

    /**
     * Retrieve data from the database.
     * @param  array $columns
     * @param  array $options['orderBy', 'direction', 'filters']
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieve(array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, $options)->get();
    }

    /**
     * Retrieve data paginated from the database.
     * @param  array $columns
     * @param  array $options['orderBy', 'direction', 'filters']
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrievePaginate(array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, $options)->paginate(intval($options['filters']['perPage'] ?? $this->model->getPerPage()));
    }

    /**
     * Retrieve all conditioning data from database.
     * @param  array $conditions
     * @param  array $columns
     * @param  array $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveBy(array $conditions, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, $options)->where($conditions)->get();
    }

    /**
     * Retrieve conditioning data paginated from database.
     * @param  array  $conditions
     * @param  array  $columns
     * @param  array $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveByPaginate(array $conditions, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, $options)->where($conditions)->paginate($options['filters']['perPage'] ?? $this->model->getPerPage());
    }

    /**
     * Retrieve all optional conditioning data from database.
     * @param  array  $conditions
     * @param  array  $orConditions
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveByOptional(array $conditions, array $orConditions, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, $options)->where($conditions)->orWhere($orConditions)->get();
    }

    /**
     * Retrieve optional conditioning data paginated from database.
     * @param  array  $conditions
     * @param  array  $orConditions
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveByOptionalPaginate(array $conditions, array $orConditions, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, $options)->where($conditions)->orWhere($orConditions)->paginate($options['filters']['perPage'] ?? $this->model->getPerPage());
    }

    /**
     * Retrieve all data with their defined relation from the database
     * @param  array  $relations
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveWithRelations(array $relations, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, $options)->with($relations)->get();
    }

    /**
     * Retrieve all data with their defined relation with conditions from the database
     * @param  array  $conditions
     * @param  array  $relations
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveWithRelationsBy(array $conditions, array $relations, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, $options)->with($relations)->where($conditions)->get();
    }

    /**
     * Retrieve data with their defined relation with conditions paginated from the database
     * @param  array  $conditions
     * @param  array  $relations
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveWithRelationsByPaginate(array $conditions, array $relations, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, $options)->with($relations)->where($conditions)->paginate($options['filters']['perPage'] ?? $this->model->getPerPage());
    }

    /**
     * Retrieve data with their defined relation paginated from the database
     * @param  array  $relations
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveWithRelationsPaginate(array $relations, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, $options)->with($relations)->paginate($options['filters']['perPage'] ?? $this->model->getPerPage());
    }

    /**
     * Retrieve all joined data ASC from the database
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveJoined(array $joins, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, Arr::add($options, 'joins', $joins))->get();
    }

    /**
     * Retrieve joined data paginated from the database
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveJoinedPaginate(array $joins, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, Arr::add($options, 'joins', $joins))->paginate($options['filters']['perPage'] ?? $this->model->getPerPage());
    }

    /**
     * Retrieve all conditioning joined data from the database
     * @param  array  $conditions
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function retrieveJoinedBy(array $conditions, array $joins, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, Arr::add($options, 'joins', $joins))->where($conditions)->get();
    }

    /**
     * Retrieve all conditioning joined data from the database
     * @param  array  $conditions
     * @param  array  $joins
     * @param  array  $columns
     * @param  array  $options
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function retrieveJoinedByPaginate(array $conditions, array $joins, array $columns = ['*'], array $options = []): LengthAwarePaginator
    {
        return $this->retrieveQuery($columns, Arr::add($options, 'joins', $joins))->where($conditions)->paginate($options['filters']['perPage'] ?? $this->model->getPerPage());
    }

    /**
     * Retrieve all data where id in array from database.
     * @param  array $ids
     * @param  array $columns
     * @param  array $options
     * @return Collection
     */
    public function retrieveByIds(array $ids, array $columns = ['*'], array $options = []): Collection
    {
        return $this->retrieveQuery($columns, $options)->whereIn($this->prefixTable('id'), $ids)->get();
    }

    /**
     * Find the given id.
     * @param string $id
     * @param array $columns
     * @param bool $trashed
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
     */
    public function find(string $id, array $columns = ['*'], bool $trashed = false)
    {
        $query = $this->select($columns);
        if (method_exists($this->model, 'withTrashed')) {
            $query->withTrashed($trashed);
        }

        return $query->find($id);
    }

    /**
     * Find by condition.
     * @param array $conditions
     * @param array $columns
     * @param bool $trashed
     * @return \Illuminate\Database\Eloquent\Model|object|static|null
     */
    public function findBy(array $conditions, array $columns = ['*'], bool $trashed = false)
    {
        $query = $this->select($columns);
        if (method_exists($this->model, 'withTrashed')) {
            $query->withTrashed($trashed);
        }

        return $query->where($conditions)->first();
    }

    /**
     * Find the given id or fail.
     * @param string $id
     * @param array $columns
     * @param bool $trashed
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findOrFail(string $id, array $columns = ['*'], bool $trashed = false): Model
    {
        $query = $this->select($columns);
        if (method_exists($this->model, 'withTrashed')) {
            $query->withTrashed($trashed);
        }

        return $query->findOrFail($id);
    }

    /**
     * Find or fail by condition.
     * @param array $conditions
     * @param array $columns
     * @param bool $trashed
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findOrFailBy(array $conditions, array $columns = ['*'], bool $trashed = false): Model
    {
        $query = $this->select($columns);
        if (method_exists($this->model, 'withTrashed')) {
            $query->withTrashed($trashed);
        }

        return $query->where($conditions)->firstOrFail();
    }

    /**
     * Create new record.
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data): Model
    {
        return $this->model->create($data);
    }

    /**
     * Create new record.
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createMany(array $data): bool
    {
        return $this->model->insert($data);
    }

    /**
     * Update the given record id.
     * @param string $id
     * @param array $data
     * @return Model
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function update(string $id, array $data)
    {
        $model = $this->findOrFail($id);

        return $model->update($data, ['upsert' => true]) ? $model : false;
    }

    /**
     * Update the given records ids.
     * @param array $ids
     * @param array $data
     * @return int
     */
    public function updateMany(array $ids, array $data): int
    {
        return $this->model->whereIn($this->model->getKeyName(), $ids)->update($data);
    }

    /**
     * Update data by conditions.
     * @param array $conditions
     * @param array $data
     * @return int
     */
    public function updateBy(array $conditions, array $data): int
    {
        return $this->model->where($conditions)->update($data);
    }

    /**
     * Destroy the given record id.
     * @param string $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function destroy(string $id):? bool
    {
        $model = $this->findOrFail($id);

        return $model->delete();
    }

    /**
     * Destroy the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function destroyMany(array $ids): bool
    {
        return $this->builder()->whereIn($this->model->getKeyName(), $ids)->delete();
    }

    /**
     * Restore the given record id.
     * @param string $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function restore(string $id):? bool
    {
        $model = $this->findOrFail($id, [$this->model->getKeyName()], true);

        return $model->restore();
    }

    /**
     * Restore the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function restoreMany(array $ids): bool
    {
        return $this->builder()->whereIn($this->model->getKeyName(), $ids)->restore();
    }

    /**
     * Force Destroy the given record id.
     * @param string $id
     * @return boolean|null
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function forceDestroy(string $id):? bool
    {
        $model = $this->findOrFail($id, [$this->model->getKeyName()], true);

        return $model->forceDelete();
    }

    /**
     * Force Destroy the given records list ids.
     * @param array $ids
     * @return boolean
     */
    public function forceDestroyMany(array $ids): bool
    {
        return $this->builder()->whereIn($this->model->getKeyName(), $ids)->forceDelete();
    }

    /**
     * Checker method to search for the given method on model before throws an exception.
     * @param  string $method
     * @param  mixed  $arguments
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        if (method_exists($this->model, $method)) {
            return $this->model->{$method}(...$arguments);
        }

        throw new \Exception(sprintf("Can't find method (%s) on %s or its model ", $method, static::class));
    }

    /**
     * Try to get the given dependency
     * @param  string $dependency
     * @return mixed
     */
    public function __get($dependency)
    {
        if (!method_exists($this, $dependency)) {
            throw new \Exception(sprintf(
                "Call undefined (%s) property. [Tip] try to use setModel() method in your constructor.",
                $dependency
            ));
        }

        return $this->{$dependency}();
    }
}
