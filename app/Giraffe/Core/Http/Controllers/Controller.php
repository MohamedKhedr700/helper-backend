<?php

namespace Giraffe\Core\Http\Controllers;

use Giraffe\Core\Traits\FractalBuilder;
use Giraffe\Core\Traits\ResponseBuilder;
use Illuminate\Routing\Controller as BaseController;

abstract class Controller extends BaseController
{
    use ResponseBuilder, FractalBuilder;

    /**
     * @OA\Info(
     *      version="3.0.0",
     *      title="Giraffe API Documentation",
     *      description="",
     *      @OA\Contact(
     *          email="info@giraffe.com"
     *      ),
     *     @OA\License(
     *         name="Apache 2.0",
     *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *     )
     * )
     */

    /**
     * @OA\Tag(
     *   name="Auth",
     *   description="Operations about Authentication",
     * )
     * @OA\Tag(
     *   name="Setting",
     *   description="Operations about setting"
     * )
     * @OA\Tag(
     *   name="User",
     *   description="Operations about user"
     * )
     * @OA\Tag(
     *   name="Role",
     *   description="Manage roles and permissions."
     * )
     * @OA\Tag(
     *   name="Team",
     *   description="Manage Teams module."
     * )
     *  @OA\Tag(
     *   name="Product",
     *   description="Operations about product"
     * )
     *  @OA\Tag(
     *   name="Tag",
     *   description="Operations about product tags"
     * )
     *  @OA\Tag(
     *   name="Wishlist",
     *   description="Operations about wishlist"
     * )
     *  @OA\Tag(
     *   name="Cart",
     *   description="Operations about cart"
     * )
     *  @OA\Tag(
     *   name="Category",
     *   description="Operations about category"
     * )
     *  @OA\Tag(
     *   name="Store",
     *   description="Operations about store"
     * )
     *   @OA\Tag(
     *   name="MobileUser",
     *   description="Operations about MobileUser"
     * )
     *   @OA\Tag(
     *   name="Promocode",
     *   description="Operations about Promocode"
     * )
     *   @OA\Tag(
     *   name="Order",
     *   description="Operations about Order"
     * )
     *   @OA\Tag(
     *   name="userAddress",
     *   description="Operations about userAddress"
     * )
     *   @OA\Tag(
     *   name="Payment",
     *   description="Operations about Payment"
     * )
     *   @OA\Tag(
     *   name="Preferences",
     *   description="Operations about UserCategory"
     * )
     *  @OA\Tag(
     *   name="Blog",
     *   description="Operations about Blog"
     * )
     *  @OA\Tag(
     *   name="Home",
     *   description="Operations about Home"
     * )
     */
}
