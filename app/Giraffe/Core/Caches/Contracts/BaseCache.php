<?php

namespace Giraffe\Core\Caches\Contracts;

use Giraffe\Core\Repositories\Contracts\BaseRepository;

interface BaseCache extends BaseRepository
{
    public function clearCache();
}
