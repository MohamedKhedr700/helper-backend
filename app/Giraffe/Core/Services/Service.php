<?php

namespace Giraffe\Core\Services;

use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Giraffe\User\Jobs\UserLoginJob;
use Giraffe\Core\Caches\Contracts\BaseCache;
use Giraffe\Core\Services\Contracts\BaseService;
use Giraffe\Core\Repositories\Contracts\BaseRepository;

abstract class Service implements BaseService
{
    /**
     * Repository class.
     * @var \Giraffe\Core\Repositories\Contracts\BaseRepository
     */
    private $repository;

    /**
     * Cache class.
     * @var \Giraffe\Core\Caches\Contracts\BaseCache
     */
    private $cache;

    /**
     * Set repository.
     * @param \Giraffe\Core\Repositories\Contracts\BaseRepository $repository
     */
    public function setRepo(BaseRepository $repository): void
    {
        $this->repository = $repository;
    }

    /**
     * Get the repository object.
     * @return \Giraffe\Core\Repositories\Contracts\BaseRepository
     */
    public function repo(): BaseRepository
    {
        return $this->repository;
    }

    /**
     * Set cache.
     * @param \Giraffe\Core\Caches\Contracts\BaseCache $cache
     */
    public function setCache(BaseCache $cache): void
    {
        $this->cache = $cache;
    }

    /**
     * Get the cache object.
     * @return \Giraffe\Core\Caches\Contracts\BaseCache
     */
    public function cache(): BaseCache
    {
        return $this->cache;
    }

    /**
     * Try to find the called method in repository layer
     * @param  string $method
     * @param  array $arguments
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        if (method_exists($this->repo(), $method)) {
            return $this->repo()->{$method}(...$arguments);
        }

        throw new \Exception(sprintf("Can't find method (%s) on %s class.", $method, static::class));
    }

    /**
     * Try to get the given dependency
     * @param  string $dependency
     * @return mixed
     */
    public function __get($dependency)
    {
        if (!method_exists($this, $dependency)) {
            throw new \Exception(sprintf(
                "Call undefined (%s) property. [Tip] try to use setRepo() or setCache() methods in your constructor.",
                $dependency
            ));
        }

        return $this->{$dependency}();
    }

    protected function getPassportToken($user, $provider)
    {
        try {
            $client   = Client::where(['personal_access_client' => 0, 'provider' => $provider])->firstOrFail();
            $response = (new \GuzzleHttp\Client)->post(route('passport.token'), [
                'form_params' => [
                    'grant_type'    => 'password',
                    'client_id'     => $client->id,
                    'client_secret' => $client->secret,
                    'username'      => Request('email') ?? $user->email ?? $user->mobile ?? $user->device_id,
                    'password'      => Request('password') ?? $user->email ?? $user->mobile ?? $user->device_id,
                    'scope'         => '',
                ],
                'curl' => [
                    CURLOPT_RESOLVE => [
                        preg_replace("(^https?://)", "", url('/')) . ':80:127.0.0.1',
                        preg_replace("(^https?://)", "", url('/')) . ':443:127.0.0.1',
                    ],
                ],
            ]);

            $data           = json_decode($response->getBody()->getContents(), true);
            $data['userId'] = $user->id;
        } catch (\Exception $e) {
            $data = false;
        } finally {
            if ($provider == 'users') {
                UserLoginJob::dispatch($user, auth()->id());
            }

            return $data;
        }
    }
}
