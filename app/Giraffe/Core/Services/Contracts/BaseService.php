<?php

namespace Giraffe\Core\Services\Contracts;

use Giraffe\Core\Caches\Contracts\BaseCache;

interface BaseService
{
    /**
     * Set cache.
     * @param \Giraffe\Core\Caches\Contracts\BaseCache $cache
     */
    public function setCache(BaseCache $cache): void;

    /**
     * Get the cache object.
     * @return \Giraffe\Core\Caches\Contracts\BaseCache
     */
    public function cache(): BaseCache;
}
