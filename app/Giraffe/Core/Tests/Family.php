<?php

namespace Giraffe\Core\Tests;

use EloquentFilter\Filterable;
use Giraffe\Core\Tests\ModelFilters\FamilyFilter;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Family extends Eloquent
{
    use Filterable;

    protected $table = 'families';

    protected $guarded = [];

    public $timestamps = false;

    /**
     * Set product filtration class.
     * @return string
     */
    public function modelFilter(): string
    {
        return $this->provideFilter(FamilyFilter::class);
    }

    public function animal()
    {
        return $this->hasMany(Animal::class, 'family_id');
    }
}
