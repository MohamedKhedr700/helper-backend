<?php

namespace Giraffe\Core\Tests;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Giraffe\Core\Tests\ModelFilters\AnimalJoinFilter;

class AnimalJoin extends Eloquent
{
    use Filterable;

    protected $table = 'animals';

    protected $guarded = [];

    /**
     * Set product filtration class.
     * @return string
     */
    public function modelFilter(): string
    {
        return $this->provideFilter(AnimalJoinFilter::class);
    }

    public function scopeTranslations($query)
    {
        return $query->join('animal_translations', 'animals.id', '=', 'animal_translations.animal_id');
    }

    public function scopeFamily($query)
    {
        return $query->join('families', 'animals.family_id', '=', 'families.id');
    }
}
