<?php

namespace Giraffe\Core\Tests;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Giraffe\Core\Tests\ModelFilters\AnimalTranslationFilter;

class AnimalTranslation extends Eloquent
{
    use Filterable;

    protected $table = 'animal_translations';

    protected $guarded = [];

    public $timestamps = false;

    /**
     * Set product filtration class.
     * @return string
     */
    public function modelFilter(): string
    {
        return $this->provideFilter(AnimalTranslationFilter::class);
    }

    public function animal()
    {
        return $this->belongsTo(Animal::class, 'animal_id');
    }
}
