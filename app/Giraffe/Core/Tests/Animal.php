<?php

namespace Giraffe\Core\Tests;

use EloquentFilter\Filterable;
use Giraffe\Core\Tests\ModelFilters\AnimalFilter;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Animal extends Eloquent
{
    use Filterable;

    protected $table = 'animals';

    protected $guarded = [];

    protected $hidden = ['updated_at', 'deleted_at'];

    protected function serializeDate(\DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * Set product filtration class.
     * @return string
     */
    public function modelFilter(): string
    {
        return $this->provideFilter(AnimalFilter::class);
    }

    public function translations()
    {
        return $this->hasMany(AnimalTranslation::class, 'animal_id');
    }

    public function family()
    {
        return $this->belongsTo(Family::class, 'family_id');
    }
}
