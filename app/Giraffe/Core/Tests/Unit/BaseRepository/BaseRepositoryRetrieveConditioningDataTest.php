<?php

namespace Giraffe\Core\Tests\Unit\BaseRepository;

use Tests\TestCase;
use Giraffe\Core\Tests\Animal;
use Giraffe\Core\Tests\AnimalJoin;
use Giraffe\Core\Repositories\Repository;
use Giraffe\Core\Tests\AnimalTranslation;
use Illuminate\Database\Eloquent\Model as Eloquent;

class BaseRepositoryRetrieveConditioningDataTest extends TestCase
{
    protected $concrete;

    public function setUp(): void
    {
        parent::setUp();

        $this->concrete = new class(new Animal()) extends Repository {
        };

        $this->createSchema();
    }

    /**
     * Setup the database schema.
     *
     * @return void
     */
    public function createSchema()
    {
        $this->schema()->create('animals', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('family');
            $table->timestamps();
            $table->softDeletes();
        });
        // it just an example, if you ask why name or family not in translation table : )
        $this->schema()->create('animal_translations', function ($table) {
            $table->increments('id');
            $table->unsignedBigInteger('animal_id');
            $table->string('details');
            $table->enum('language', ['en', 'ar']);
        });
    }

    /**
     * Tear down the database schema.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        $this->schema()->drop('animals');
        $this->schema()->drop('animal_translations');
    }

    public function testRetrieveByMethodCanReturnSpecificColumns()
    {
        $data = [
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => now()->addDay()->toDateTimeString()],
            ['id' => 3, 'name' => 'Wolf', 'family' => 'Canis lupus', 'created_at' => now()->addDays(2)->toDateTimeString()],
        ];

        Animal::insert($data);

        $animal_data = $this->concrete->retrieveBy(
            [
                ['name', 'like', '%lion%'],
            ],
            ['name', 'family']
        )->first()->toArray();

        $this->assertArrayNotHasKey('id', $animal_data);
    }

    public function testRetrieveByMethodByDefaultReturnsConditioningDataOrderedAscendingByCreateTime()
    {
        $data = [
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => now()->addDay()->toDateTimeString()],
            ['id' => 3, 'name' => 'Wolf', 'family' => 'Canis lupus', 'created_at' => now()->addDays(2)->toDateTimeString()],
        ];

        Animal::insert($data);

        $animal_data = $this->concrete->retrieveBy([
            ['family', 'like', '%canis%'],
        ], ['*'], ['filters' => ['sort' => 'id', 'direction' => 'asc']])->makeHidden(['translations'])->toArray();

        $data = collect($data)->forget(0)->values()->all();

        $this->assertEquals($data, $animal_data);
    }

    public function testRetrieveByMethodReturnsTranslatedDataAutomaticallyByEagerLoadingIfExist()
    {
        Animal::insert([
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae'],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis'],
            ['id' => 3, 'name' => 'Wolf', 'family' => 'Canis lupus'],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a four strong animal.', 'language' => 'en'],
        ]);

        $animal_data = $this->concrete->retrieveBy([
            ['family', 'like', '%canis%'],
        ])->toArray();

        $this->assertArrayHasKey('translations', $animal_data[0]);
        $this->assertCount(2, $animal_data);
    }

    public function testRetrieveByMethodReturnsTranslatedDataAutomaticallyThroughTranslationsScopeMethodIfNoEagerLoadingFound()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae'],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis'],
            ['id' => 3, 'name' => 'Wolf', 'family' => 'Canis lupus'],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a four strong animal.', 'language' => 'en'],
        ]);

        $animal_data = $this->concrete->retrieveBy([
            ['family', 'like', '%canis%'],
        ])->toArray();

        $this->assertArrayHasKey('details', $animal_data[0]);
        $this->assertCount(2, $animal_data);
    }

    public function testRetrieveLatestByMethodCanReturnSpecificColumns()
    {
        $data = [
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => now()->addDay()->toDateTimeString()],
            ['id' => 3, 'name' => 'Wolf', 'family' => 'Canis lupus', 'created_at' => now()->addDays(2)->toDateTimeString()],
        ];

        Animal::insert($data);

        $animal_data = $this->concrete->retrieveBy(
            [
                ['name', 'like', '%lion%'],
            ],
            ['name', 'family'],
            ['filters' => ['sort' => 'id', 'direction' => 'desc']]
        )->first()->toArray();

        $this->assertArrayNotHasKey('id', $animal_data);
    }

    public function testRetrieveLatestByMethodByDefaultReturnsConditioningDataOrderedDescendingByCreateTime()
    {
        $data = [
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => now()->addDay()->toDateTimeString()],
            ['id' => 3, 'name' => 'Wolf', 'family' => 'Canis lupus', 'created_at' => now()->addDays(2)->toDateTimeString()],
        ];

        Animal::insert($data);

        $animal_data = $this->concrete->retrieveBy([
            ['family', 'like', '%canis%'],
        ], ['*'], ['filters' => [
            'sort'      => 'id',
            'direction' => 'desc',
        ]])->makeHidden(['translations'])->toArray();

        $data = collect($data)->forget(0)->sortByDesc('created_at')->values()->all();

        $this->assertEquals($data, $animal_data);
    }

    public function testRetrieveLatestByMethodReturnsTranslatedDataAutomaticallyByEagerLoadingIfExist()
    {
        Animal::insert([
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae'],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis'],
            ['id' => 3, 'name' => 'Wolf', 'family' => 'Canis lupus'],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a four strong animal.', 'language' => 'en'],
        ]);

        $animal_data = $this->concrete->retrieveBy([
            ['family', 'like', '%Canis%'],
        ], ['*'], ['filters' => [
            'sort'      => 'id',
            'direction' => 'desc',
        ]])->toArray();

        $this->assertArrayHasKey('translations', $animal_data[0]);
        $this->assertCount(2, $animal_data);
    }

    public function testRetrieveLatestByMethodReturnsTranslatedDataAutomaticallyThroughTranslationsScopeMethodIfNoEagerLoadingFound()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae'],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis'],
            ['id' => 3, 'name' => 'Wolf', 'family' => 'Canis lupus'],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a four strong animal.', 'language' => 'en'],
        ]);

        $animal_data = $this->concrete->retrieveBy([
            ['family', 'like', '%Canis%'],
        ], ['*'], ['filters' => [
            'sort'      => 'animal_id',
            'direction' => 'desc',
        ]])->toArray();

        $this->assertArrayHasKey('details', $animal_data[0]);
        $this->assertCount(2, $animal_data);
    }

    public function testRetrieveByOptionalMethodCanReturnSpecificColumns()
    {
        $data = [
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => now()->addDay()->toDateTimeString()],
            ['id' => 3, 'name' => 'Wolf', 'family' => 'Canis lupus', 'created_at' => now()->addDays(2)->toDateTimeString()],
        ];

        Animal::insert($data);

        $animal_data = $this->concrete->retrieveByOptional(
            [
                ['name', 'like', '%dog%'],
            ],
            [
                ['family', 'like', '%Canis%'],
            ],
            ['name', 'family']
        )->first()->makeHidden(['translations'])->toArray();

        $this->assertArrayNotHasKey('id', $animal_data);
    }

    public function testRetrieveByOptionalMethodByDefaultReturnsConditioningDataOrderedAscendingByCreateTime()
    {
        $data = [
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => now()->addDay()->toDateTimeString()],
            ['id' => 3, 'name' => 'Moon', 'family' => 'Canis lupus', 'created_at' => now()->addDays(2)->toDateTimeString()],
        ];

        Animal::insert($data);

        $animal_data = $this->concrete->retrieveByOptional(
            [
                ['name', 'like', '%dog%'],
            ],
            [
                ['family', 'like', '%Canis%'],
            ],
            ['*'],
            ['filters' => ['sort' => 'id', 'direction' => 'asc']]
        )->makeHidden(['translations'])->toArray();

        $data = collect($data)->forget(0)->values()->all();

        $this->assertEquals($data, $animal_data);
    }

    public function testRetrieveByOptionalMethodReturnsTranslatedDataAutomaticallyByEagerLoadingIfExist()
    {
        Animal::insert([
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae'],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis'],
            ['id' => 3, 'name' => 'Moon', 'family' => 'Canis lupus'],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a four strong animal.', 'language' => 'en'],
        ]);

        $animal_data = $this->concrete->retrieveByOptional(
            [
                ['name', 'like', '%dog%'],
            ],
            [
                ['family', 'like', '%Canis%'],
            ]
        )->toArray();

        $this->assertArrayHasKey('translations', $animal_data[0]);
        $this->assertCount(2, $animal_data);
    }

    public function testRetrieveByOptionalMethodReturnsTranslatedDataAutomaticallyThroughTranslationsScopeMethodIfNoEagerLoadingFound()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae'],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis'],
            ['id' => 3, 'name' => 'Moon', 'family' => 'Canis lupus'],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a four strong animal.', 'language' => 'en'],
        ]);

        $animal_data = $this->concrete->retrieveByOptional(
            [
                ['name', 'like', '%dog%'],
            ],
            [
                ['family', 'like', '%Canis lupus%'],
            ]
        )->toArray();

        $this->assertArrayHasKey('details', $animal_data[0]);
        $this->assertCount(2, $animal_data);
    }

    public function testRetrieveLatestByOptionalMethodCanReturnSpecificColumns()
    {
        $data = [
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => now()->addDay()->toDateTimeString()],
            ['id' => 3, 'name' => 'Wolf', 'family' => 'Canis lupus', 'created_at' => now()->addDays(2)->toDateTimeString()],
        ];

        Animal::insert($data);

        $animal_data = $this->concrete->retrieveByOptional(
            [
                ['name', 'like', '%dog%'],
            ],
            [
                ['family', 'like', '%Canis%'],
            ],
            ['name', 'family'],
            ['filters' => ['sort' => 'id', 'direction' => 'desc']]
        )->first()->toArray();

        $this->assertArrayNotHasKey('id', $animal_data);
    }

    public function testRetrieveLatestByOptionalMethodByDefaultReturnsConditioningDataOrderedDescendingByCreateTime()
    {
        $data = [
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae', 'created_at' => now()->toDateTimeString()],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis', 'created_at' => now()->addDay()->toDateTimeString()],
            ['id' => 3, 'name' => 'Moon', 'family' => 'Canis lupus', 'created_at' => now()->addDays(2)->toDateTimeString()],
        ];

        Animal::insert($data);

        $animal_data = $this->concrete->retrieveByOptional(
            [
                ['name', 'like', '%dog%'],
            ],
            [
                ['family', 'like', '%Canis%'],
            ],
            ['*'],
            ['filters' => ['sort' => 'id', 'direction' => 'desc']]
        )->makeHidden(['translations'])->toArray();

        $data = collect($data)->forget(0)->sortByDesc('created_at')->values()->all();

        $this->assertEquals($data, $animal_data);
    }

    public function testRetrieveLatestByOptionalMethodReturnsTranslatedDataAutomaticallyByEagerLoadingIfExist()
    {
        Animal::insert([
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae'],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis'],
            ['id' => 3, 'name' => 'Moon', 'family' => 'Canis lupus'],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a four strong animal.', 'language' => 'en'],
        ]);

        $animal_data = $this->concrete->retrieveByOptional(
            [
                ['name', 'like', '%dog%'],
            ],
            [
                ['family', 'like', '%Canis%'],
            ],
            ['*'],
            ['filters' => ['sort' => 'id', 'direction' => 'desc']]
        )->toArray();

        $this->assertArrayHasKey('translations', $animal_data[0]);
        $this->assertCount(2, $animal_data);
    }

    public function testRetrieveLatestByOptionalMethodReturnsTranslatedDataAutomaticallyThroughTranslationsScopeMethodIfNoEagerLoadingFound()
    {
        $this->concrete = new class(new AnimalJoin()) extends Repository {
        };

        AnimalJoin::insert([
            ['id' => 1, 'name' => 'Lion', 'family' => 'Felidae'],
            ['id' => 2, 'name' => 'Dog', 'family' => 'Canis'],
            ['id' => 3, 'name' => 'Moon', 'family' => 'Canis lupus'],
        ]);

        AnimalTranslation::insert([
            ['animal_id' => 1, 'details' => 'This is a five strong animal.', 'language' => 'en'],
            ['animal_id' => 2, 'details' => 'This is a three strong animal.', 'language' => 'en'],
            ['animal_id' => 3, 'details' => 'This is a four strong animal.', 'language' => 'en'],
        ]);

        $animal_data = $this->concrete->retrieveByOptional(
            [
                ['name', 'like', '%dog%'],
            ],
            [
                ['family', 'like', '%Canis%'],
            ],
            ['*'],
            ['filters' => ['sort' => 'animal_id', 'direction' => 'desc']]
        )->toArray();

        $this->assertArrayHasKey('details', $animal_data[0]);
        $this->assertCount(2, $animal_data);
    }

    /**
     * Get a database connection instance.
     *
     * @return \Illuminate\Database\Connection
     */
    protected function connection()
    {
        return Eloquent::getConnectionResolver()->connection();
    }

    /**
     * Get a schema builder instance.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    protected function schema()
    {
        return $this->connection()->getSchemaBuilder();
    }
}
