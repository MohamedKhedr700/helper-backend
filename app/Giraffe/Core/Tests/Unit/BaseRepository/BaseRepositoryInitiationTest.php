<?php

namespace Giraffe\Core\Tests\Unit\BaseRepository;

use Tests\TestCase;
use Giraffe\Core\Tests\Animal;
use Giraffe\Core\Repositories\Repository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Giraffe\Core\Repositories\Contracts\BaseRepository;

class BaseRepositoryInitiationTest extends TestCase
{
    protected $concrete;

    public function setUp(): void
    {
        parent::setUp();

        $this->concrete = new class(new Animal()) extends Repository {
        };

        $this->createSchema();
    }

    /**
     * Setup the database schema.
     *
     * @return void
     */
    public function createSchema()
    {
        $this->schema()->create('animals', function ($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('family');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Tear down the database schema.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        $this->schema()->drop('animals');
    }

    public function testExtendedConcreteClassIsAnInstanceOfBaseRepositoryInterface()
    {
        $this->assertInstanceOf(
            BaseRepository::class,
            $this->concrete
        );
    }

    public function testGetMagicMethodThrowsAnExceptionWhenCallPropertyNotCorrespondedToAnyMethodNameInRepository()
    {
        $this->expectException(\Exception::class);

        $this->concrete->anyRandomMethodName;
    }

    /**
     * Get a database connection instance.
     *
     * @return \Illuminate\Database\Connection
     */
    protected function connection()
    {
        return Eloquent::getConnectionResolver()->connection();
    }

    /**
     * Get a schema builder instance.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    protected function schema()
    {
        return $this->connection()->getSchemaBuilder();
    }
}
