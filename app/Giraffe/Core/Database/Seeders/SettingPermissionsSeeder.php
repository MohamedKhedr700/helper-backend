<?php

namespace Giraffe\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Giraffe\Role\Models\Permission;
use Giraffe\Role\Models\PermissionTranslation;

class SettingPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->allPermission();
        $this->readPermission();
        $this->updatePermission();
    }

    public function allPermission()
    {
        if (!Permission::where('name', 'all-setting')->count()) {
            $permission = Permission::factory()->create([
                'name'   => 'all-setting',
                'module' => 'setting',
                'order'  => '800',

            ]);

            PermissionTranslation::factory()->create([
                'permission_id'      => $permission->id,
                'display_name'       => 'All Setting',
                'description'        => 'All Setting Permissions.',
                'module_name'        => 'setting',
                'language'           => 'en',
            ]);

            PermissionTranslation::factory()->create([
                'permission_id'      => $permission->id,
                'display_name'       => 'إدارة جميع انواع الضبط',
                'description'        => 'إدارة جميع انواع الضبط.',
                'module_name'        => 'انواع الضبط',
                'language'           => 'ar',
            ]);
        }
    }

    public function readPermission()
    {
        if (!Permission::where('name', 'read-setting')->count()) {
            $permission = Permission::factory()->create([
                'name'       => 'read-setting',
                'module'     => 'setting',
                'is_default' => true,
                'order'      => '810',

            ]);

            PermissionTranslation::factory()->create([
                'permission_id'      => $permission->id,
                'display_name'       => 'Read Setting',
                'description'        => 'Read Setting Permission.',
                'module_name'        => 'setting',
                'language'           => 'en',
            ]);

            PermissionTranslation::factory()->create([
                'permission_id'      => $permission->id,
                'display_name'       => 'قرأة انواع الضبط',
                'description'        => 'قرأة انواع الضبط المتوفرين.',
                'module_name'        => 'انواع الضبط',
                'language'           => 'ar',
            ]);
        }
    }

    public function updatePermission()
    {
        if (!Permission::where('name', 'update-setting')->count()) {
            $permission = Permission::factory()->create([
                'name'   => 'update-setting',
                'module' => 'setting',
                'order'  => '820',

            ]);

            PermissionTranslation::factory()->create([
                'permission_id'      => $permission->id,
                'display_name'       => 'Edit Setting',
                'description'        => 'Edit Setting Permission.',
                'module_name'        => 'setting',
                'language'           => 'en',
            ]);

            PermissionTranslation::factory()->create([
                'permission_id'      => $permission->id,
                'display_name'       => 'تحديث نوع ضبط',
                'description'        => 'تحديث نوع ضبط ',
                'module_name'        => 'انواع الضبط',
                'language'           => 'ar',
            ]);
        }
    }
}
