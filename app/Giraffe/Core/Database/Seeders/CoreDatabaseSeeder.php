<?php

namespace Giraffe\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CoreDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @SuppressWarnings("StaticAccess")
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(SettingPermissionsSeeder::class);
    }
}
