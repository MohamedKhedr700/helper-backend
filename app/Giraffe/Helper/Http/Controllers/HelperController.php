<?php

namespace Giraffe\Helper\Http\Controllers;

use Giraffe\Helper\Http\Requests\StoreHelperRequest;
use Giraffe\Helper\Services\HelperService;
use http\Env;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Giraffe\Core\Http\Controllers\Controller;

class HelperController extends Controller
{
  public $service;

  public function __construct(HelperService $service)
  {
      $this->service = $service;

  }

  public function store(StoreHelperRequest $request)
  {
      return $this->service->create($request->only(['jobTitle', 'firstName', 'lastName', 'email', 'mobile', 'notes', 'to'])) ?
          $this->returnSuccess(trans('messages.create.success', ['module' => trans('helper::helpers.helper')])) :
          $this->returnBadRequest(config('helper.error-codes.create-fail'), trans('messages.create.fail', ['module' => trans('helper::helpers.helper')]));
  }
}
