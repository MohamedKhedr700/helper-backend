<?php

namespace Giraffe\Helper\Repositories;

use Giraffe\Core\Repositories\Repository;
use Giraffe\Helper\Repositories\Contracts\HelperRepository;

class HelperRepositoryEloquent extends Repository implements HelperRepository
{
}
