<?php

namespace Giraffe\Helper\Repositories\Contracts;

use Giraffe\Core\Repositories\Contracts\BaseRepository;

interface HelperRepository extends BaseRepository
{
}
