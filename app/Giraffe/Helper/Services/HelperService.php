<?php

namespace Giraffe\Helper\Services;

use ArrayKeysCaseTransform\ArrayKeys;
use Giraffe\Core\Services\Service;
use Giraffe\Helper\Emails\HelperMail;
use Giraffe\Helper\Repositories\Contracts\HelperRepository;
use Illuminate\Support\Facades\Mail;

class HelperService extends Service
{
    public function __construct(HelperRepository $repository)
    {
        $this->setRepo($repository);
    }

    public function create(array $data)
    {
        $mail = ["mirette@giraffecode.com","m.fathy@wecanagency.com","Info@marota-eg.com"];
//        $obj = $this->repo()->create(ArrayKeys::toSnakeCase($data));
        Mail::to($mail)->send(new HelperMail($data));
        return true;
    }
}
