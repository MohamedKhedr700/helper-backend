<?php

namespace Giraffe\Helper\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class HelperMail extends Mailable
{
    use Queueable, SerializesModels;
protected $helper;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($helper)
    {
    $this->helper = $helper;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('helper')
            ->with([
                'firstName' => $this->helper['firstName'],
                'lastName' => $this->helper['lastName'],
                'email' => $this->helper['email'],
                'mobile' => $this->helper['mobile'],
                'notes' => array_key_exists('notes', $this->helper) ? $this->helper['notes'] : null,
                'jobTitle'  => array_key_exists('jobTitle',$this->helper) ? $this->helper['jobTitle'] : null,
            ]);
    }
}
