<?php

namespace Giraffe\Helper\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Helper extends Model
{
    public $incrementing = false;
    protected $keyType = ['string'];
    protected $fillable = [
        'first_name', 'last_name', 'email', 'mobile', 'notes', 'job_title',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function($model) {
            $model->id = str::uuid()->toString();
        });
    }
}
