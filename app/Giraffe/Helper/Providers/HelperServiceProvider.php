<?php

namespace Giraffe\Helper\Providers;

use Illuminate\Support\ServiceProvider;
use Giraffe\Helper\Models\Helper;
use Illuminate\Database\Eloquent\Factories\Factory;
use Giraffe\Helper\Caches\HelperCacheRedis;
use Giraffe\Helper\Repositories\HelperRepositoryEloquent;
use Giraffe\Helper\Repositories\Contracts\HelperRepository;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->app->bind(HelperRepository::class, function () {
            $repository = new HelperRepositoryEloquent(new Helper());

            return (config('app.cache')) ? $repository : new HelperCacheRedis($repository);
        });
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('helper.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'helper'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/helper');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/helper';
        }, \Config::get('view.paths')), [$sourcePath]), 'helper');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/helper');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'helper');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'helper');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production')) {
            $this->app->singleton(Factory::class, function () {
                return Factory::construct(__DIR__ . '/Database/factories');
            });
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
