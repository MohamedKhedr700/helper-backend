<?php

namespace Giraffe\Helper\Caches\Contracts;

use Giraffe\Core\Caches\Contracts\BaseCache;

interface HelperCache extends BaseCache
{
}
