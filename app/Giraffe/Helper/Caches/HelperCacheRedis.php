<?php

namespace Giraffe\Helper\Caches;

use Giraffe\Core\Caches\Cache;
use Giraffe\Helper\Caches\Contracts\HelperCache;
use Giraffe\Helper\Repositories\Contracts\HelperRepository;

class HelperCacheRedis extends Cache implements HelperCache, HelperRepository
{
    /**
     * @var HelperRepository
     */
    protected $repository;

    public function __construct(HelperRepository $helperRepository)
    {
        parent::__construct();

        $this->entityName = config('helper.name');
        $this->repository = $helperRepository;
    }
}
