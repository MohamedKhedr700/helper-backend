<?php

use Nwidart\Modules\Activators\FileActivator;

return [

    /*
    |--------------------------------------------------------------------------
    | Module Namespace
    |--------------------------------------------------------------------------
    |
    | Default module namespace.
    |
    */

    'namespace' => 'Giraffe',

    /*
    |--------------------------------------------------------------------------
    | Module Stubs
    |--------------------------------------------------------------------------
    |
    | Default module stubs.
    |
    */

    'stubs' => [
        'enabled' => true,
        'path'    => base_path() . '/app/Console/laravel-modules/stubs',
        'files'   => [
            'routes/web'                      => 'Routes/web.php',
            'routes/api'                      => 'Routes/api.php',
            'scaffold/config'                 => 'Config/config.php',
            'composer'                        => 'composer.json',
            'webpack'                         => 'webpack.mix.js',
            'package'                         => 'package.json',
            'services/service'                => 'Services/IndustryService.php',
            'repositories/repository'         => 'Repositories/IndustryRepositoryEloquent.php',
            'repositories/contracts/contract' => 'Repositories/Contracts/IndustryRepository.php',
            'caches/cache'                    => 'Caches/IndustryCacheRedis.php',
            'caches/contracts/contract'       => 'Caches/Contracts/IndustryCache.php',
        ],
        'replacements' => [
            'routes/web'      => ['LOWER_NAME', 'STUDLY_NAME'],
            'routes/api'      => ['LOWER_NAME'],
            'webpack'         => ['LOWER_NAME'],
            'json'            => ['LOWER_NAME', 'STUDLY_NAME', 'MODULE_NAMESPACE'],
            'scaffold/config' => ['STUDLY_NAME'],
            'composer'        => [
                'LOWER_NAME',
                'STUDLY_NAME',
                'VENDOR',
                'AUTHOR_NAME',
                'AUTHOR_EMAIL',
                'MODULE_NAMESPACE',
            ],
            'services/service'                => ['MODULE_NAMESPACE', 'STUDLY_NAME'],
            'repositories/repository'         => ['MODULE_NAMESPACE', 'STUDLY_NAME'],
            'repositories/contracts/contract' => ['MODULE_NAMESPACE', 'STUDLY_NAME'],
            'caches/cache'                    => ['MODULE_NAMESPACE', 'LOWER_NAME', 'STUDLY_NAME'],
            'caches/contracts/contract'       => ['MODULE_NAMESPACE', 'STUDLY_NAME'],
        ],
        'gitkeep' => true,
    ],
    'paths' => [
        /*
        |--------------------------------------------------------------------------
        | Modules path
        |--------------------------------------------------------------------------
        |
        | This path used for save the generated module. This path also will be added
        | automatically to list of scanned folders.
        |
        */

        'modules' => base_path('app/Giraffe'),
        /*
        |--------------------------------------------------------------------------
        | Modules assets path
        |--------------------------------------------------------------------------
        |
        | Here you may update the modules assets path.
        |
        */

        'assets' => public_path('modules'),
        /*
        |--------------------------------------------------------------------------
        | The migrations path
        |--------------------------------------------------------------------------
        |
        | Where you run 'module:publish-migration' command, where do you publish the
        | the migration files?
        |
        */

        'migration' => base_path('database/migrations'),
        /*
        |--------------------------------------------------------------------------
        | Generator path
        |--------------------------------------------------------------------------
        | Customise the paths where the folders will be generated.
        | Set the generate key to false to not generate that folder
        */
        'generator' => [
            'config'        => ['path' => 'Config', 'generate' => true],
            'command'       => ['path' => 'Console', 'generate' => false],
            'migration'     => ['path' => 'Database/Migrations', 'generate' => true],
            'seeder'        => ['path' => 'Database/Seeders', 'generate' => true],
            'factory'       => ['path' => 'Database/Factories', 'generate' => true],
            'filter'        => ['path' => 'Http/Middleware', 'generate' => true],
            'provider'      => ['path' => 'Providers', 'generate' => true],
            'assets'        => ['path' => 'Resources/assets', 'generate' => false],
            'lang'          => ['path' => 'Resources/lang', 'generate' => true],
            'views'         => ['path' => 'Resources/views', 'generate' => true],
            'test'          => ['path' => 'Tests/Unit', 'generate' => true],
            'test-feature'  => ['path' => 'Tests/Feature', 'generate' => true],
            'event'         => ['path' => 'Events', 'generate' => false],
            'listener'      => ['path' => 'Listeners', 'generate' => false],
            'policies'      => ['path' => 'Policies', 'generate' => false],
            'rules'         => ['path' => 'Rules', 'generate' => false],
            'jobs'          => ['path' => 'Jobs', 'generate' => false],
            'emails'        => ['path' => 'Emails', 'generate' => false],
            'notifications' => ['path' => 'Notifications', 'generate' => false],
            //===== ODM layers =====//
            // API
            'controller' => ['path' => 'Http/Controllers', 'generate' => true],
            'resource'   => ['path' => 'Http/Transformers', 'generate' => true],
            'request'    => ['path' => 'Http/Requests', 'generate' => true],
            // Logic
            'service'            => ['path' => 'Services', 'generate' => true],
            // Database
            'repository'           => ['path' => 'Repositories', 'generate' => true],
            'repository-contracts' => ['path' => 'Repositories/Contracts', 'generate' => true],
            'cache'                => ['path' => 'Caches', 'generate' => true],
            'cache-contracts'      => ['path' => 'Caches/Contracts', 'generate' => true],
            'model'                => ['path' => 'Models', 'generate' => true],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Package commands
    |--------------------------------------------------------------------------
    |
    | Here you can define which commands will be visible and used in your
    | application. If for example you don't use some of the commands provided
    | you can simply comment them out.
    |
    */
    'commands' => [
        CommandMakeCommand::class,
        ControllerMakeCommand::class,
        DisableCommand::class,
        DumpCommand::class,
        EnableCommand::class,
        EventMakeCommand::class,
        JobMakeCommand::class,
        ListenerMakeCommand::class,
        MailMakeCommand::class,
        MiddlewareMakeCommand::class,
        NotificationMakeCommand::class,
        ProviderMakeCommand::class,
        RouteProviderMakeCommand::class,
        InstallCommand::class,
        ListCommand::class,
        ModuleDeleteCommand::class,
        ModuleMakeCommand::class,
        FactoryMakeCommand::class,
        PolicyMakeCommand::class,
        RequestMakeCommand::class,
        RuleMakeCommand::class,
        MigrateCommand::class,
        MigrateRefreshCommand::class,
        MigrateResetCommand::class,
        MigrateRollbackCommand::class,
        MigrateStatusCommand::class,
        MigrationMakeCommand::class,
        ModelMakeCommand::class,
        PublishCommand::class,
        PublishConfigurationCommand::class,
        PublishMigrationCommand::class,
        PublishTranslationCommand::class,
        SeedCommand::class,
        SeedMakeCommand::class,
        SetupCommand::class,
        UnUseCommand::class,
        UpdateCommand::class,
        UseCommand::class,
        ResourceMakeCommand::class,
        TestMakeCommand::class,
        LaravelModulesV6Migrator::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Scan Path
    |--------------------------------------------------------------------------
    |
    | Here you define which folder will be scanned. By default will scan vendor
    | directory. This is useful if you host the package in packagist website.
    |
    */

    'scan' => [
        'enabled' => false,
        'paths'   => [
            base_path('vendor/*/*'),
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Composer File Template
    |--------------------------------------------------------------------------
    |
    | Here is the config for composer.json file, generated by this package
    |
    */

    'composer' => [
        'vendor' => 'giraffe',
        'author' => [
            'name'  => 'Giraffe Company',
            'email' => 'info@giraffe.com',
        ],
    ],

    'composer-output' => false,

    /*
    |--------------------------------------------------------------------------
    | Caching
    |--------------------------------------------------------------------------
    |
    | Here is the config for setting up caching feature.
    |
    */
    'cache' => [
        'enabled'  => false,
        'key'      => 'giraffe-modules',
        'lifetime' => 60,
    ],
    /*
    |--------------------------------------------------------------------------
    | Choose what laravel-modules will register as custom namespaces.
    | Setting one to false will require you to register that part
    | in your own Service Provider class.
    |--------------------------------------------------------------------------
    */
    'register' => [
        'translations' => true,
        /**
         * load files on boot or register method
         *
         * Note: boot not compatible with asgardcms
         *
         * @example boot|register
         */
        'files' => 'register',
    ],

    /*
    |--------------------------------------------------------------------------
    | Activators
    |--------------------------------------------------------------------------
    |
    | You can define new types of activators here, file, database etc. The only
    | required parameter is 'class'.
    | The file activator will store the activation status in storage/installed_modules
    */
    'activators' => [
        'file' => [
            'class'          => FileActivator::class,
            'statuses-file'  => base_path('modules_statuses.json'),
            'cache-key'      => 'activator.installed',
            'cache-lifetime' => 604800,
        ],
    ],

    'activator' => 'file',
];
